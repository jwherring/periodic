from distutils.core import setup

setup(
  name = 'periodic',
  packages = ['periodic'],
  version = '0.1',
  description = 'Periodic API Python client',
  author = 'Joshua Herring',
  author_email = 'josh@periodic.is',
  url = 'https://jwherring@bitbucket.org/jwherring/periodic.git', 
  download_url = 'https://bitbucket.org/jwherring/periodic/get/current.tar.bz2', 
  keywords = ['scheduling', 'booking', 'inventory', 'api', 'client', 'apiclient'], 
  classifiers = [],
)
